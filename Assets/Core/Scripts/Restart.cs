﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public class Restart : MonoBehaviour
    {
        public void CloseApp()
        {
            Application.Quit();
        }
    }
}