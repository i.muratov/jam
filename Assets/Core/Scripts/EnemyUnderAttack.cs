﻿using System;
using UnityEngine;

namespace Core.Scripts
{
    public class EnemyUnderAttack : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("EnemyDetector") && !other.gameObject.CompareTag("Enemy01") && !other.gameObject.CompareTag("Enemy02") && !other.gameObject.CompareTag("Enemy03"))
            {
                if (gameObject.CompareTag("Enemy01"))
                {
                    if (Curse.Dragon01ConnectWith == 1) DragonAttack.Dragon01Enemy = null;
                    else if (Curse.Dragon02ConnectWith == 1) DragonAttack.Dragon02Enemy = null;
                    else if (Curse.Dragon03ConnectWith == 1) DragonAttack.Dragon03Enemy = null;
                }
                else if (gameObject.CompareTag("Enemy02"))
                {
                    if (Curse.Dragon01ConnectWith == 2) DragonAttack.Dragon01Enemy = null;
                    else if (Curse.Dragon02ConnectWith == 2) DragonAttack.Dragon02Enemy = null;
                    else if (Curse.Dragon03ConnectWith == 2) DragonAttack.Dragon03Enemy = null;
                }
                else if (gameObject.CompareTag("Enemy03"))
                {
                    if (Curse.Dragon01ConnectWith == 3) DragonAttack.Dragon01Enemy = null;
                    else if (Curse.Dragon02ConnectWith == 3) DragonAttack.Dragon02Enemy = null;
                    else if (Curse.Dragon03ConnectWith == 3) DragonAttack.Dragon03Enemy = null;
                }

                EnemySpawn.EnemyCounter = EnemySpawn.EnemyCounter - 1;
                
                Explosion.GetExplosion(gameObject.transform.position);
                
                other.gameObject.SetActive(false);
                gameObject.SetActive(false);
            }
        }
    }
}