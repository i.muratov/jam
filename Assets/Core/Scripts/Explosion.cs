﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public class Explosion : MonoBehaviour
    {
        [SerializeField] private GameObject explosion;
        [SerializeField] private GameObject curseExplosion;
        [SerializeField] private int explosionCount;
        
        private static readonly List<GameObject> explosionPool = new List<GameObject>();
        private static readonly List<GameObject> curseExplosionPool = new List<GameObject>();

        private void Start()
        {
            for (int i = 0; i < explosionCount; i++)
            {
                GameObject newExplosion = Instantiate(explosion);
                explosionPool.Add(newExplosion);
            }
            
            for (int i = 0; i < explosionCount; i++)
            {
                GameObject newCurseExplosion = Instantiate(curseExplosion);
                curseExplosionPool.Add(newCurseExplosion);
            }
        }
        
        public static void GetExplosion(Vector3 explosionPosition)
        {
            for (int i = 0; i < explosionPool.Count; i++)
            {
                if (!explosionPool[i].activeInHierarchy)
                {
                    explosionPool[i].transform.position = explosionPosition;
                    explosionPool[i].SetActive(true);
                    
                    break;
                }
            }
        }
        
        public static void GetCurseExplosion(Vector3 explosionPosition)
        {
            for (int i = 0; i < curseExplosionPool.Count; i++)
            {
                if (!curseExplosionPool[i].activeInHierarchy)
                {
                    curseExplosionPool[i].transform.position = explosionPosition;
                    curseExplosionPool[i].SetActive(true);
                    
                    break;
                }
            }
        }
    }
}


