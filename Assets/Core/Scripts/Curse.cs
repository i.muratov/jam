﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Scripts
{
    public class Curse : MonoBehaviour
    {
        public static readonly List<int> PoolCurseDragon01 = new List<int>();
        public static readonly List<int> PoolCurseDragon02 = new List<int>();
        public static readonly List<int> PoolCurseDragon03 = new List<int>();
        public static int Dragon01ConnectWith = 1;
        public static int Dragon02ConnectWith = 2;
        public static int Dragon03ConnectWith = 3;

        private static void ApplyCurse()
        {
            Debug.Log("CurseCount: " + PoolCurseDragon01.Count + ", " + PoolCurseDragon02.Count+ ", " + PoolCurseDragon03.Count);
            
            switch (PoolCurseDragon01.Count)
            {
                case 1:
                    
                    DragonMove.Dragon01Animator.SetInteger("CurseCount", 1);

                    break;
                
                case 2:
                    
                    DragonMove.Dragon01Animator.SetInteger("CurseCount", 2);

                    break;
                
                case 3:

                    DragonMove.isDragon01Dead = true;

                    break;
            }
            
            switch (PoolCurseDragon02.Count)
            {
                case 1:
                    
                    DragonMove.Dragon02Animator.SetInteger("CurseCount", 1);

                    break;
                
                case 2:
                    
                    DragonMove.Dragon02Animator.SetInteger("CurseCount", 2);

                    break;
                
                case 3:
                    
                    DragonMove.isDragon02Dead = true;

                    break;
            }
            
            switch (PoolCurseDragon03.Count)
            {
                case 1:
                    
                    DragonMove.Dragon03Animator.SetInteger("CurseCount", 1);

                    break;
                
                case 2:
                    
                    DragonMove.Dragon03Animator.SetInteger("CurseCount", 2);

                    break;
                
                case 3:
                    
                    DragonMove.isDragon03Dead = true;

                    break;
            }
        }

        public static void GenerateCurse()
        {
            int randomDragon = Random.Range(1, 4);
            int randomCurse = Random.Range(1, 6);

            if (randomCurse == 3)
            {
                while (randomCurse == 3)
                {
                    randomCurse = Random.Range(1, 6);
                }
            }

            switch (randomDragon)
            {
                case 1:
                    
                    if (!PoolCurseDragon01.Contains(randomCurse) && PoolCurseDragon01.Count < 3)
                    {
                        Debug.Log(randomCurse);
                        
                        PoolCurseDragon01.Add(randomCurse);
                        
                        Explosion.GetCurseExplosion(DragonMove.Dragon01Transform.position);

                        ApplyCurse();

                        switch (randomCurse)
                        {
                            case 3:
                                
                                int temp;
                            
                                if (Dragon02ConnectWith == 1)
                                {
                                    temp = Dragon01ConnectWith;
                                    Dragon01ConnectWith = Dragon03ConnectWith;
                                    Dragon03ConnectWith = temp;
                                }
                                else if (Dragon03ConnectWith == 1)
                                {
                                    temp = Dragon01ConnectWith;
                                    Dragon01ConnectWith = Dragon02ConnectWith;
                                    Dragon02ConnectWith = temp;
                                }
                                else
                                {
                                    int randomConnection = Random.Range(1, 3);

                                    switch (randomConnection)
                                    {
                                        case 1:

                                            temp = Dragon01ConnectWith;
                                            Dragon01ConnectWith = Dragon02ConnectWith;
                                            Dragon02ConnectWith = temp;

                                            break;
                                
                                        case 2:

                                            temp = Dragon01ConnectWith;
                                            Dragon01ConnectWith = Dragon03ConnectWith;
                                            Dragon03ConnectWith = temp;
                                    
                                            break;
                                    }
                                }

                                break;
                            
                            case 4:

                                DragonMove.CurseTimeCounterDragon01 = DragonMove.SleepingCurseDuration;

                                break;
                        }
                    }
                    else
                    {
                        GenerateCurse();
                    }

                    break;
                
                case 2:
                    
                    if (!PoolCurseDragon02.Contains(randomCurse) && PoolCurseDragon02.Count < 3)
                    {
                        PoolCurseDragon02.Add(randomCurse);
                        
                        Explosion.GetCurseExplosion(DragonMove.Dragon02Transform.position);
                        
                        ApplyCurse();

                        switch (randomCurse)
                        {
                            case 3:
                                
                                int temp;
                            
                                if (Dragon01ConnectWith == 2)
                                {
                                    temp = Dragon02ConnectWith;
                                    Dragon02ConnectWith = Dragon03ConnectWith;
                                    Dragon03ConnectWith = temp;
                                }
                                else if (Dragon03ConnectWith == 2)
                                {
                                    temp = Dragon02ConnectWith;
                                    Dragon02ConnectWith = Dragon01ConnectWith;
                                    Dragon01ConnectWith = temp;
                                }
                                else
                                {
                                    int randomConnection = Random.Range(1, 3);

                                    switch (randomConnection)
                                    {
                                        case 1:

                                            temp = Dragon02ConnectWith;
                                            Dragon02ConnectWith = Dragon01ConnectWith;
                                            Dragon01ConnectWith = temp;

                                            break;
                                
                                        case 2:

                                            temp = Dragon02ConnectWith;
                                            Dragon02ConnectWith = Dragon03ConnectWith;
                                            Dragon03ConnectWith = temp;
                                    
                                            break;
                                    }
                                }

                                break;
                            
                            case 4:
                                
                                DragonMove.CurseTimeCounterDragon02 = DragonMove.SleepingCurseDuration;

                                break;
                        }
                    }
                    else
                    {
                        GenerateCurse();
                    }
                    
                    break;
                
                case 3:
                    
                    if (!PoolCurseDragon03.Contains(randomCurse) && PoolCurseDragon03.Count < 3)
                    {
                        PoolCurseDragon03.Add(randomCurse);
                        
                        Explosion.GetCurseExplosion(DragonMove.Dragon03Transform.position);
                        
                        ApplyCurse();

                        switch (randomCurse)
                        {
                            case 3:
                                
                                int temp;
                            
                                if (Dragon01ConnectWith == 3)
                                {
                                    temp = Dragon03ConnectWith;
                                    Dragon03ConnectWith = Dragon02ConnectWith;
                                    Dragon02ConnectWith = temp;
                                }
                                else if (Dragon02ConnectWith == 3)
                                {
                                    temp = Dragon03ConnectWith;
                                    Dragon03ConnectWith = Dragon01ConnectWith;
                                    Dragon01ConnectWith = temp;
                                }
                                else
                                {
                                    int randomConnection = Random.Range(1, 3);

                                    switch (randomConnection)
                                    {
                                        case 1:

                                            temp = Dragon03ConnectWith;
                                            Dragon03ConnectWith = Dragon01ConnectWith;
                                            Dragon01ConnectWith = temp;

                                            break;
                                
                                        case 2:

                                            temp = Dragon03ConnectWith;
                                            Dragon03ConnectWith = Dragon02ConnectWith;
                                            Dragon02ConnectWith = temp;
                                    
                                            break;
                                    }
                                }

                                break;
                            
                            case 4:
                                
                                DragonMove.CurseTimeCounterDragon03 = DragonMove.SleepingCurseDuration;

                                break;
                        }
                    }
                    else
                    {
                        GenerateCurse();
                    }

                    break;
            }
        }
    }
}