﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class CurseUi : MonoBehaviour
    {
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private Text text;
        [SerializeField] private Image dragon01Health01;
        [SerializeField] private Image dragon01Health02;
        [SerializeField] private Image dragon01Health03;
        [SerializeField] private Image dragon02Health01;
        [SerializeField] private Image dragon02Health02;
        [SerializeField] private Image dragon02Health03;
        [SerializeField] private Image dragon03Health01;
        [SerializeField] private Image dragon03Health02;
        [SerializeField] private Image dragon03Health03;
        [SerializeField] private Image curse01;
        [SerializeField] private Image curse02;
        [SerializeField] private Sprite brokenHeart;
        [SerializeField] private Sprite curseIcon01;
        [SerializeField] private Sprite curseIcon02;
        [SerializeField] private Sprite curseIcon03;
        [SerializeField] private Sprite curseIcon04;
        [SerializeField] private Sprite curseIcon05;

        public static Text Text;

        private void Awake()
        {
            Text = text;
        }

        private void FixedUpdate()
        {
            switch (Curse.PoolCurseDragon01.Count)
            {
                case 1:
                    
                    dragon01Health01.sprite = brokenHeart;

                    break;
                
                case 2:
                    
                    dragon01Health02.sprite = brokenHeart;

                    break;
                
                case 3:
                    
                    dragon01Health03.sprite = brokenHeart;

                    break;
            }
            
            switch (Curse.PoolCurseDragon02.Count)
            {
                case 1:
                    
                    dragon02Health01.sprite = brokenHeart;

                    break;
                
                case 2:
                    
                    dragon02Health02.sprite = brokenHeart;

                    break;
                
                case 3:
                    
                    dragon02Health03.sprite = brokenHeart;

                    break;
            }
            
            switch (Curse.PoolCurseDragon03.Count)
            {
                case 1:
                    
                    dragon03Health01.sprite = brokenHeart;

                    break;
                
                case 2:
                    
                    dragon03Health02.sprite = brokenHeart;

                    break;
                
                case 3:
                    
                    dragon03Health03.sprite = brokenHeart;

                    break;
            }

            if (ChooseDragon.DragonUnderControl == 1)
            {
                if (Curse.PoolCurseDragon01.Count > 0)
                {
                    curse01.color = Color.white;
                    
                    switch (Curse.PoolCurseDragon01[0])
                    {
                        case 1:

                            curse01.sprite = curseIcon01;

                            break;
                    
                        case 2:
                        
                            curse01.sprite = curseIcon02;

                            break;
                    
                        case 3:
                        
                            curse01.sprite = curseIcon03;

                            break;
                    
                        case 4:
                        
                            curse01.sprite = curseIcon04;

                            break;
                    
                        case 5:
                        
                            curse01.sprite = curseIcon05;

                            break;
                    }

                    if (Curse.PoolCurseDragon01.Count > 1)
                    {
                        curse02.color = Color.white;
                        
                        switch (Curse.PoolCurseDragon01[1])
                        {
                            case 1:

                                curse02.sprite = curseIcon01;

                                break;
                    
                            case 2:
                        
                                curse02.sprite = curseIcon02;

                                break;
                    
                            case 3:
                        
                                curse02.sprite = curseIcon03;

                                break;
                    
                            case 4:
                        
                                curse02.sprite = curseIcon04;

                                break;
                    
                            case 5:
                        
                                curse02.sprite = curseIcon05;

                                break;
                        }
                    }
                }
                else
                {
                    curse01.color = Color.clear;
                    curse02.color = Color.clear;
                }
            }
            
            if (ChooseDragon.DragonUnderControl == 2)
            {
                if (Curse.PoolCurseDragon02.Count > 0)
                {
                    curse01.color = Color.white;
                    
                    switch (Curse.PoolCurseDragon02[0])
                    {
                        case 1:

                            curse01.sprite = curseIcon01;

                            break;
                    
                        case 2:
                        
                            curse01.sprite = curseIcon02;

                            break;
                    
                        case 3:
                        
                            curse01.sprite = curseIcon03;

                            break;
                    
                        case 4:
                        
                            curse01.sprite = curseIcon04;

                            break;
                    
                        case 5:
                        
                            curse01.sprite = curseIcon05;

                            break;
                    }

                    if (Curse.PoolCurseDragon02.Count > 1)
                    {
                        curse02.color = Color.white;
                        
                        switch (Curse.PoolCurseDragon02[1])
                        {
                            case 1:

                                curse02.sprite = curseIcon01;

                                break;
                    
                            case 2:
                        
                                curse02.sprite = curseIcon02;

                                break;
                    
                            case 3:
                        
                                curse02.sprite = curseIcon03;

                                break;
                    
                            case 4:
                        
                                curse02.sprite = curseIcon04;

                                break;
                    
                            case 5:
                        
                                curse02.sprite = curseIcon05;

                                break;
                        }
                    }
                }
                else
                {
                    curse01.color = Color.clear;
                    curse02.color = Color.clear;
                }
            }
            
            if (ChooseDragon.DragonUnderControl == 3)
            {
                if (Curse.PoolCurseDragon03.Count > 0)
                {
                    curse01.color = Color.white;
                    
                    switch (Curse.PoolCurseDragon03[0])
                    {
                        case 1:

                            curse01.sprite = curseIcon01;

                            break;
                    
                        case 2:
                        
                            curse01.sprite = curseIcon02;

                            break;
                    
                        case 3:
                        
                            curse01.sprite = curseIcon03;

                            break;
                    
                        case 4:
                        
                            curse01.sprite = curseIcon04;

                            break;
                    
                        case 5:
                        
                            curse01.sprite = curseIcon05;

                            break;
                    }

                    if (Curse.PoolCurseDragon03.Count > 1)
                    {
                        curse02.color = Color.white;
                        
                        switch (Curse.PoolCurseDragon03[1])
                        {
                            case 1:

                                curse02.sprite = curseIcon01;

                                break;
                    
                            case 2:
                        
                                curse02.sprite = curseIcon02;

                                break;
                    
                            case 3:
                        
                                curse02.sprite = curseIcon03;

                                break;
                    
                            case 4:
                        
                                curse02.sprite = curseIcon04;

                                break;
                    
                            case 5:
                        
                                curse02.sprite = curseIcon05;

                                break;
                        }
                    }
                }
                else
                {
                    curse01.color = Color.clear;
                    curse02.color = Color.clear;
                }
            }

            if (DragonMove.isGameOver && !gameOverPanel.activeInHierarchy)
            {
                gameOverPanel.SetActive(true);
                gameOverPanel.GetComponent<Animator>().Play("FadeOn");
                StartCoroutine(Over());
            }
        }

        private IEnumerator Over()
        {
            yield return new WaitForSeconds(1.0f);
            
            gameOverPanel.transform.GetChild(0).gameObject.SetActive(true);
            gameOverPanel.transform.GetChild(1).gameObject.SetActive(true);
        }

        public static void ChangeText(string message)
        {
            Text.text = message;
            Text.GetComponent<Animator>().Play("Text");
        }
    } 
}