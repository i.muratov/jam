﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Scripts
{
    public class EnemySpawn : MonoBehaviour
    {
        [SerializeField] private GameObject enemy;
        [SerializeField] private Transform[] enemySpawn;
        [SerializeField] private Transform[] enemyTurn;
        [SerializeField] private Transform enemyFinish;
        [SerializeField] private Material enemyMaterial01;
        [SerializeField] private Material enemyMaterial02;
        [SerializeField] private Material enemyMaterial03;
        [SerializeField] private float enemySpeed;
        [SerializeField] private int enemyCount;
        [SerializeField] private int enemyCountMax;
        [SerializeField] private float enemyCountMultiplier;
        [SerializeField] private float enemySpawnDelay; 
        [SerializeField] private int waveToStartWith;
        [SerializeField] private float waveTimeBetween;
        [SerializeField] private int waveToUseSpawn02;
        [SerializeField] private int waveToUseSpawn03;
        [SerializeField] private int waveToUseSpawn04;
        [SerializeField] private int waveToUseSpawn05;

        public static double WaveCounter;
        public static double EnemyCounter;
        
        private readonly List<GameObject> poolEnemy = new List<GameObject>();
        
        private double turn01X;
        private double turn01Z;
        private double turn02X;
        private double turn02Z;
        private double turn03X;
        private double turn03Z;
        private double turn04X;
        private double turn04Z;
        private double turn05X;
        private double turn05Z;
        private double turn06X;
        private double turn06Z;
        private double aimX;
        private double aimZ;

        private void Awake()
        {
            for (int i = 0; i < enemyCountMax; i++)
            {
                GameObject newEnemy = Instantiate(enemy);
                int enemyType = Random.Range(1, 4);

                switch (enemyType)
                {
                    case 1:

                        newEnemy.tag = "Enemy01";
                        newEnemy.transform.GetChild(0).GetComponent<Renderer>().material = enemyMaterial01;

                        break;
                    
                    case 2:
                        
                        newEnemy.tag = "Enemy02";
                        newEnemy.transform.GetChild(0).GetComponent<Renderer>().material = enemyMaterial02;

                        break;
                    
                    case 3:
                        
                        newEnemy.tag = "Enemy03";
                        newEnemy.transform.GetChild(0).GetComponent<Renderer>().material = enemyMaterial03;

                        break;
                }
                
                poolEnemy.Add(newEnemy);
            }

            turn01X = Math.Round(enemyTurn[0].position.x, 1);
            turn01Z = Math.Round(enemyTurn[0].position.z, 1);
            turn02X = Math.Round(enemyTurn[1].position.x, 1);
            turn02Z = Math.Round(enemyTurn[1].position.z, 1);
            turn03X = Math.Round(enemyTurn[2].position.x, 1);
            turn03Z = Math.Round(enemyTurn[2].position.z, 1);
            turn04X = Math.Round(enemyTurn[3].position.x, 1);
            turn04Z = Math.Round(enemyTurn[3].position.z, 1);
            turn05X = Math.Round(enemyTurn[4].position.x, 1);
            turn05Z = Math.Round(enemyTurn[4].position.z, 1);
            turn06X = Math.Round(enemyTurn[5].position.x, 1);
            turn06Z = Math.Round(enemyTurn[5].position.z, 1);
            
            aimX = Math.Round(enemyFinish.position.x, 1);
            aimZ = Math.Round(enemyFinish.position.z, 1);
            
            WaveCounter = waveToStartWith;
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < poolEnemy.Count; i++)
            {
                if (poolEnemy[i].activeInHierarchy)
                {
                    double currentX = Math.Round(poolEnemy[i].transform.position.x, 1);
                    double currentZ = Math.Round(poolEnemy[i].transform.position.z, 1);
                    
                    if (currentX == turn01X && currentZ == turn01Z && poolEnemy[i].transform.rotation.y == 0.0f)
                    {
                        int random = Random.Range(1, 3);

                        switch (random)
                        {
                            case 1:
                                
                                poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.forward * enemySpeed;
                                poolEnemy[i].transform.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);

                                break;
                            
                            case 2:
                                
                                poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.back * enemySpeed;
                                poolEnemy[i].transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);

                                break;
                        }
                    }
                    else if (currentX == turn02X && currentZ == turn02Z || currentX == turn03X && currentZ == turn03Z || currentX == turn06X && currentZ == turn06Z)
                    {
                        poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.right * enemySpeed;
                        poolEnemy[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    }
                    else if (currentX == turn04X && currentZ == turn04Z)
                    {
                        poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.back * enemySpeed;
                        poolEnemy[i].transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
                    }
                    else if (currentX == turn05X && currentZ == turn05Z)
                    {
                        poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.forward * enemySpeed;
                        poolEnemy[i].transform.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
                    }
                    else if (currentX == aimX && currentZ == aimZ)
                    {
                        if (poolEnemy[i].activeInHierarchy)
                        {
                            Explosion.GetExplosion(poolEnemy[i].transform.position);
                            
                            poolEnemy[i].SetActive(false);

                            Curse.GenerateCurse();
                            
                            EnemyCounter = EnemyCounter - 1;
                        }
                    }
                }
            }

            if (DragonMove.isGameOver)
            {
                for (int i = 0; i < poolEnemy.Count; i++)
                {
                    if (poolEnemy[i].activeInHierarchy)
                    {
                        poolEnemy[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
                    }
                }
            }
            else if (EnemyCounter < 1 && Tutorial.IsTutorialOver)
            {
                StartCoroutine(SpawnWave());
            }
        }

        private IEnumerator SpawnWave()
        {
            double countEnemy = Math.Round(enemyCount * WaveCounter * enemyCountMultiplier);
            double countEnemyTemp = 0;

            EnemyCounter = countEnemy;
            
            Debug.Log("WAVE: " + WaveCounter);

            //string text = "WAVE: " + WaveCounter;

            CurseUi.ChangeText("WAVE: " + WaveCounter);
            
            yield return new WaitForSeconds(waveTimeBetween);

            while (countEnemyTemp != countEnemy)
            {
                Vector3 spawn = enemySpawn[0].position;
                Vector3 direction = Vector3.right;

                if (WaveCounter < waveToUseSpawn02)
                {
                    spawn = enemySpawn[0].position;
                }
                else if (WaveCounter < waveToUseSpawn03)
                {
                    int random = Random.Range(1, 3);

                    switch (random)
                    {
                        case 1:
                            
                            spawn = enemySpawn[0].position;

                            break;
                        
                        case 2:
                            
                            spawn = enemySpawn[1].position;

                            break;
                    }
                }
                else if (WaveCounter < waveToUseSpawn04)
                {
                    int random = Random.Range(1, 4);

                    switch (random)
                    {
                        case 1:
                            
                            spawn = enemySpawn[0].position;

                            break;
                        
                        case 2:
                            
                            spawn = enemySpawn[1].position;

                            break;
                        
                        case 3:
                            
                            spawn = enemySpawn[2].position;

                            break;
                    }
                }
                else if (WaveCounter < waveToUseSpawn05)
                {
                    int random = Random.Range(1, 5);

                    switch (random)
                    {
                        case 1:
                            
                            spawn = enemySpawn[0].position;

                            break;
                        
                        case 2:
                            
                            spawn = enemySpawn[1].position;

                            break;
                        
                        case 3:
                            
                            spawn = enemySpawn[2].position;

                            break;
                        
                        case 4:
                            
                            spawn = enemySpawn[3].position;
                            direction = Vector3.back;

                            break;
                    }
                }
                else
                {
                    int random = Random.Range(1, 6);

                    switch (random)
                    {
                        case 1:
                            
                            spawn = enemySpawn[0].position;

                            break;
                        
                        case 2:
                            
                            spawn = enemySpawn[1].position;

                            break;
                        
                        case 3:
                            
                            spawn = enemySpawn[2].position;

                            break;
                        
                        case 4:
                            
                            spawn = enemySpawn[3].position;
                            direction = Vector3.back;

                            break;
                        
                        case 5:
                            
                            spawn = enemySpawn[4].position;
                            direction = Vector3.forward;

                            break;
                    }
                }

                Shuffle();
                
                Spawn(spawn, direction);
                
                countEnemyTemp++;
                
                yield return new WaitForSeconds(enemySpawnDelay);
            }

            WaveCounter++;
        }

        private void Spawn(Vector3 spawn, Vector3 direction)
        {
            for (int i = 0; i < poolEnemy.Count; i++)
            {
                if (!poolEnemy[i].activeInHierarchy)
                {
                    poolEnemy[i].transform.position = spawn;
                    poolEnemy[i].transform.rotation = Quaternion.identity;
                    poolEnemy[i].SetActive(true);

                    poolEnemy[i].GetComponent<Rigidbody>().velocity = direction * enemySpeed;
                    
                    break;
                }
            }
        }
        
        private void Shuffle()
        {
            for (int i = 0; i < poolEnemy.Count; i++)
            {
                GameObject temp = poolEnemy[i];
                int randomIndex = Random.Range(i, poolEnemy.Count);
                poolEnemy[i] = poolEnemy[randomIndex];
                poolEnemy[randomIndex] = temp;
            }
        }
    }
}