﻿using UnityEngine;

namespace Core.Scripts
{
    public class ChooseDragon : MonoBehaviour
    {
        public static int DragonUnderControl = 1;
        
        public void ChooseDragon01()
        {
            Debug.Log("ChooseDragon01!");

            if (!DragonMove.isDragon01Dead)
            {
                DragonUnderControl = 1;
            }
        }
        
        public void ChooseDragon02()
        {
            Debug.Log("ChooseDragon02!");
            
            if (!DragonMove.isDragon02Dead)
            {
                DragonUnderControl = 2;
            }
        }
        
        public void ChooseDragon03()
        {
            Debug.Log("ChooseDragon03!");
            
            if (!DragonMove.isDragon03Dead)
            {
                DragonUnderControl = 3;
            }
        }
    }
}
