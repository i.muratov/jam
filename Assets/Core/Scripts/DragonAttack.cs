﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  Core.Scripts
{
    public class DragonAttack : MonoBehaviour
    {
        [SerializeField] private GameObject bullet;
        [SerializeField] private float bulletSpeed;
        [SerializeField] private int bulletCount;
        
        public static GameObject Dragon01Enemy;
        public static GameObject Dragon02Enemy;
        public static GameObject Dragon03Enemy;

        private static DragonAttack instance;
        private static readonly List<GameObject> poolBullet = new List<GameObject>();
        private static readonly List<GameObject> poolActiveBullet = new List<GameObject>();
        private static readonly List<GameObject> poolActiveEnemy = new List<GameObject>();
        private static Transform bulletSpawnDragon01;
        private static Transform bulletSpawnDragon02;
        private static Transform bulletSpawnDragon03;

        private void Awake()
        {
            instance = this;
            
            for (int i = 0; i < bulletCount; i++)
            {
                GameObject newBullet = Instantiate(bullet);
                poolBullet.Add(newBullet);
            }

            if (gameObject.CompareTag("Dragon01")) bulletSpawnDragon01 = gameObject.transform.GetChild(0).transform;
            if (gameObject.CompareTag("Dragon02")) bulletSpawnDragon02 = gameObject.transform.GetChild(0).transform;
            if (gameObject.CompareTag("Dragon03")) bulletSpawnDragon03 = gameObject.transform.GetChild(0).transform;
        }

        private void FixedUpdate()
        {
            if (gameObject.CompareTag("Dragon01") && Dragon01Enemy != null) transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Dragon01Enemy.transform.position - transform.position), 5.0f * Time.deltaTime);
            if (gameObject.CompareTag("Dragon02") && Dragon02Enemy != null) transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Dragon02Enemy.transform.position - transform.position), 5.0f * Time.deltaTime);
            if (gameObject.CompareTag("Dragon03") && Dragon03Enemy != null) transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Dragon03Enemy.transform.position - transform.position), 5.0f * Time.deltaTime);

            for (int i = 0; i < poolBullet.Count; i++)
            {
                if (poolBullet[i].activeInHierarchy)
                {
                    for (int j = 0; j < poolActiveBullet.Count; j++)
                    {
                        if (poolBullet[i] == poolActiveBullet[j] && poolBullet[i].transform.position != poolActiveEnemy[j].transform.position)
                        {
                            poolBullet[i].transform.position = Vector3.MoveTowards(poolBullet[i].transform.position, poolActiveEnemy[j].transform.position, bulletSpeed * Time.deltaTime);
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < poolActiveBullet.Count; j++)
                    {
                        if (poolBullet[i] == poolActiveBullet[j])
                        {
                            poolActiveBullet.Remove(poolActiveBullet[j]);
                            poolActiveEnemy.Remove(poolActiveEnemy[j]);
                        }
                    }
                }
            }
        }

        public static void StartAttackDragon01(GameObject enemy)
        {
            Dragon01Enemy = enemy;

            if (Curse.PoolCurseDragon01.Contains(4) && DragonMove.CurseTimeCounterDragon01 < 0.05f)
            {
                return;
            }
            else
            {
                instance.StartCoroutine(BulletFire(DragonMove.Dragon01Animator, bulletSpawnDragon01, Dragon01Enemy));
            }
        }
        
        public static void StartAttackDragon02(GameObject enemy)
        {
            Dragon02Enemy = enemy;

            if (Curse.PoolCurseDragon02.Contains(4) && DragonMove.CurseTimeCounterDragon02 < 0.05f)
            {
                return;
            }
            else
            {
                instance.StartCoroutine(BulletFire(DragonMove.Dragon02Animator, bulletSpawnDragon02, Dragon02Enemy));
            }
        }
        
        public static void StartAttackDragon03(GameObject enemy)
        {
            Dragon03Enemy = enemy;
            
            if (Curse.PoolCurseDragon03.Contains(4) && DragonMove.CurseTimeCounterDragon03 < 0.05f)
            {
                return;
            }
            else
            {
                instance.StartCoroutine(BulletFire(DragonMove.Dragon03Animator, bulletSpawnDragon03, Dragon03Enemy));
            }
        }

        private static IEnumerator BulletFire(Animator animator, Transform spawn, GameObject enemy)
        {
            yield return new WaitForSeconds(0.5f);
            
            for (int i = 0; i < poolBullet.Count; i++)
            {
                if (!poolBullet[i].activeInHierarchy)
                {
                    poolBullet[i].transform.GetChild(0).gameObject.SetActive(false);
                    poolBullet[i].transform.GetChild(1).gameObject.SetActive(false);
                    poolBullet[i].transform.GetChild(2).gameObject.SetActive(false);

                    if (animator == DragonMove.Dragon01Animator)
                    {
                        poolBullet[i].transform.GetChild(0).gameObject.SetActive(true);
                    }
                    else if (animator == DragonMove.Dragon02Animator)
                    {
                        poolBullet[i].transform.GetChild(1).gameObject.SetActive(true);
                    }
                    else if (animator == DragonMove.Dragon03Animator)
                    {
                        poolBullet[i].transform.GetChild(2).gameObject.SetActive(true);
                    }
                    
                    animator.Play("attack_1");
                    
                    DragonMove.AudioAttack.Play();
                    
                    poolBullet[i].transform.position = spawn.position;
                    poolBullet[i].SetActive(true);
                    
                    poolActiveBullet.Add(poolBullet[i]);
                    poolActiveEnemy.Add(enemy);
                    
                    break;
                }
            }
        }
    }
}