﻿using System;
using UnityEngine;

namespace Core.Scripts
{
    public class DragonMove : MonoBehaviour
    {
        [SerializeField] private GameObject dragon01;
        [SerializeField] private GameObject dragon02;
        [SerializeField] private GameObject dragon03;
        [SerializeField] private AudioSource audioMove;
        [SerializeField] private AudioSource audioAttack;
        [SerializeField] private float moveDistance;
        [SerializeField] private float moveSpeed;
        [SerializeField] private float sleepingCurseDuration;

        public static Transform Dragon01Transform;
        public static Transform Dragon02Transform;
        public static Transform Dragon03Transform;
        public static AudioSource AudioMove;
        public static AudioSource AudioAttack;
        public static Animator Dragon01Animator;
        public static Animator Dragon02Animator;
        public static Animator Dragon03Animator;
        public static Vector3 PositionCurrentDragon01;
        public static Vector3 PositionCurrentDragon02;
        public static Vector3 PositionCurrentDragon03;
        public static Vector3 PositionAimingDragon01;
        public static Vector3 PositionAimingDragon02;
        public static Vector3 PositionAimingDragon03;
        public static float CurseTimeCounterDragon01;
        public static float CurseTimeCounterDragon02;
        public static float CurseTimeCounterDragon03;
        public static float SleepingCurseDuration;
        public static bool isDragon01Dead;
        public static bool isDragon02Dead;
        public static bool isDragon03Dead;
        public static bool isGameOver;

        private Vector3 aimingPosition;
        private bool isMoving;

        private void Awake()
        {
            AudioMove = audioMove;
            AudioAttack = audioAttack;

            Dragon01Transform = dragon01.transform;
            Dragon02Transform = dragon02.transform;
            Dragon03Transform = dragon03.transform;

            Dragon01Animator = dragon01.GetComponentInChildren<Animator>();
            Dragon02Animator = dragon02.GetComponentInChildren<Animator>();
            Dragon03Animator = dragon03.GetComponentInChildren<Animator>();
            
            PositionCurrentDragon01 = dragon01.transform.position;
            PositionCurrentDragon02 = dragon02.transform.position;
            PositionCurrentDragon03 = dragon03.transform.position;

            PositionAimingDragon01 = PositionCurrentDragon01;
            PositionAimingDragon02 = PositionCurrentDragon02;
            PositionAimingDragon03 = PositionCurrentDragon03;

            SleepingCurseDuration = sleepingCurseDuration;
        }

        private void FixedUpdate()
        {
            PositionCurrentDragon01 = dragon01.transform.position;
            PositionCurrentDragon02 = dragon02.transform.position;
            PositionCurrentDragon03 = dragon03.transform.position;

            if (isMoving)
            {
                if (PositionCurrentDragon01 != PositionAimingDragon01) dragon01.transform.position = Vector3.MoveTowards(dragon01.transform.position, PositionAimingDragon01, moveSpeed * Time.deltaTime);
                if (PositionCurrentDragon02 != PositionAimingDragon02) dragon02.transform.position = Vector3.MoveTowards(dragon02.transform.position, PositionAimingDragon02, moveSpeed * Time.deltaTime);
                if (PositionCurrentDragon03 != PositionAimingDragon03) dragon03.transform.position = Vector3.MoveTowards(dragon03.transform.position, PositionAimingDragon03, moveSpeed * Time.deltaTime);
                
                switch (ChooseDragon.DragonUnderControl)
                {
                    case 1:
                        
                        if (PositionCurrentDragon01 == PositionAimingDragon01) isMoving = false;
                        
                        break;
                    
                    case 2:
                        
                        if (PositionCurrentDragon02 == PositionAimingDragon02) isMoving = false;
                        
                        break;
                    
                    case 3:
                        
                        if (PositionCurrentDragon03 == PositionAimingDragon03) isMoving = false;
                        
                        break;
                }
            }

            if (Curse.PoolCurseDragon01.Contains(4) && CurseTimeCounterDragon01 > 0.0f)
            {
                CurseTimeCounterDragon01 = CurseTimeCounterDragon01 - Time.deltaTime;
            }

            if (Curse.PoolCurseDragon02.Contains(4) && CurseTimeCounterDragon02 > 0.0f)
            {
                CurseTimeCounterDragon02 = CurseTimeCounterDragon02 - Time.deltaTime;
            }
            
            if (Curse.PoolCurseDragon03.Contains(4) && CurseTimeCounterDragon03 > 0.0f)
            {
                CurseTimeCounterDragon03 = CurseTimeCounterDragon03 - Time.deltaTime;
            }

            if (!isGameOver)
            {
                if (isDragon01Dead && dragon01.activeInHierarchy)
                {
                    Explosion.GetExplosion(dragon01.transform.position);

                    PositionCurrentDragon01 = new Vector3(50.0f, 50.0f, 50.0f);
                    PositionAimingDragon01 = new Vector3(50.0f, 50.0f, 50.0f);

                    if (!isDragon02Dead)
                    {
                        ChooseDragon.DragonUnderControl = 2;
                        
                        Curse.Dragon02ConnectWith = int.Parse(Curse.Dragon02ConnectWith.ToString() + Curse.Dragon01ConnectWith.ToString());
                    }
                    else if (!isDragon03Dead)
                    {
                        ChooseDragon.DragonUnderControl = 3;
                        
                        Curse.Dragon03ConnectWith = int.Parse(Curse.Dragon03ConnectWith.ToString() + Curse.Dragon01ConnectWith.ToString());
                    }
                    
                    Debug.Log("Curse.Dragon01ConnectWith: " + Curse.Dragon01ConnectWith);
                    Debug.Log("Curse.Dragon02ConnectWith: " + Curse.Dragon02ConnectWith);
                    Debug.Log("Curse.Dragon03ConnectWith: " + Curse.Dragon03ConnectWith);
                    
                    dragon01.SetActive(false);
                }
            
                if (isDragon02Dead && dragon02.activeInHierarchy)
                {
                    Explosion.GetExplosion(dragon02.transform.position);
                    
                    PositionCurrentDragon02 = new Vector3(50.0f, 50.0f, 50.0f);
                    PositionAimingDragon02 = new Vector3(50.0f, 50.0f, 50.0f);
                
                    if (!isDragon01Dead)
                    {
                        ChooseDragon.DragonUnderControl = 1;
                        
                        Curse.Dragon01ConnectWith = int.Parse(Curse.Dragon01ConnectWith.ToString() + Curse.Dragon02ConnectWith.ToString());
                    }
                    else if (!isDragon03Dead)
                    {
                        ChooseDragon.DragonUnderControl = 3;
                        
                        Curse.Dragon03ConnectWith = int.Parse(Curse.Dragon03ConnectWith.ToString() + Curse.Dragon02ConnectWith.ToString());
                    }
                    
                    Debug.Log("Curse.Dragon01ConnectWith: " + Curse.Dragon01ConnectWith);
                    Debug.Log("Curse.Dragon02ConnectWith: " + Curse.Dragon02ConnectWith);
                    Debug.Log("Curse.Dragon03ConnectWith: " + Curse.Dragon03ConnectWith);
                
                    dragon02.SetActive(false);
                }
            
                if (isDragon03Dead && dragon03.activeInHierarchy)
                {
                    Explosion.GetExplosion(dragon03.transform.position);
                    
                    PositionCurrentDragon03 = new Vector3(50.0f, 50.0f, 50.0f);
                    PositionAimingDragon03 = new Vector3(50.0f, 50.0f, 50.0f);

                    if (!isDragon01Dead)
                    {
                        ChooseDragon.DragonUnderControl = 1;
                        
                        Curse.Dragon01ConnectWith = int.Parse(Curse.Dragon01ConnectWith.ToString() + Curse.Dragon03ConnectWith.ToString());
                    }
                    else if (!isDragon02Dead)
                    {
                        ChooseDragon.DragonUnderControl = 2;
                        
                        Curse.Dragon02ConnectWith = int.Parse(Curse.Dragon02ConnectWith.ToString() + Curse.Dragon03ConnectWith.ToString());
                    }
                    
                    Debug.Log("Curse.Dragon01ConnectWith: " + Curse.Dragon01ConnectWith);
                    Debug.Log("Curse.Dragon02ConnectWith: " + Curse.Dragon02ConnectWith);
                    Debug.Log("Curse.Dragon03ConnectWith: " + Curse.Dragon03ConnectWith);
                    
                    dragon03.SetActive(false);
                }
                    
                if (isDragon01Dead && isDragon02Dead && isDragon03Dead)
                {
                    isGameOver = true;
                }
            }

            if (ChooseDragon.DragonUnderControl == 1 && !dragon01.transform.GetChild(3).gameObject.activeInHierarchy)
            {
                dragon01.transform.GetChild(3).gameObject.SetActive(true);
                dragon02.transform.GetChild(3).gameObject.SetActive(false);
                dragon03.transform.GetChild(3).gameObject.SetActive(false);
            }
            else if (ChooseDragon.DragonUnderControl == 2 && !dragon02.transform.GetChild(3).gameObject.activeInHierarchy)
            {
                dragon01.transform.GetChild(3).gameObject.SetActive(false);
                dragon02.transform.GetChild(3).gameObject.SetActive(true);
                dragon03.transform.GetChild(3).gameObject.SetActive(false);
            }
            else if (ChooseDragon.DragonUnderControl == 3 && !dragon03.transform.GetChild(3).gameObject.activeInHierarchy)
            {
                dragon01.transform.GetChild(3).gameObject.SetActive(false);
                dragon02.transform.GetChild(3).gameObject.SetActive(false);
                dragon03.transform.GetChild(3).gameObject.SetActive(true);
            }
        }

        public void MoveUp()
        {
            if (!isMoving)
            {
                AudioMove.Play();
                
                switch (ChooseDragon.DragonUnderControl)
                {
                    case 1:

                        if (!Curse.PoolCurseDragon01.Contains(2))
                        {
                            MoveUpDragon01();
                        }
                        else
                        {
                            MoveDownDragon01();
                        }

                        if (Curse.PoolCurseDragon01.Contains(4))
                        {
                            CurseTimeCounterDragon01 = SleepingCurseDuration;
                        }

                        break;
                
                    case 2:

                        if (!Curse.PoolCurseDragon02.Contains(2))
                        {
                            MoveUpDragon02();
                        }
                        else
                        {
                            MoveDownDragon02();
                        }
                        
                        if (Curse.PoolCurseDragon02.Contains(4))
                        {
                            CurseTimeCounterDragon02 = SleepingCurseDuration;
                        }

                        break;
                
                    case 3:

                        if (!Curse.PoolCurseDragon03.Contains(2))
                        {
                            MoveUpDragon03();
                        }
                        else
                        {
                            MoveDownDragon03();
                        }
                        
                        if (Curse.PoolCurseDragon03.Contains(4))
                        {
                            CurseTimeCounterDragon03 = SleepingCurseDuration;
                        }

                        break;
                }
            }
        }
        
        public void MoveDown()
        {
            if (!isMoving)
            {
                AudioMove.Play();
                
                switch (ChooseDragon.DragonUnderControl)
                {
                    case 1:
                        
                        if (!Curse.PoolCurseDragon01.Contains(2))
                        {
                            MoveDownDragon01();
                        }
                        else
                        {
                            MoveUpDragon01();
                        }
                        
                        if (Curse.PoolCurseDragon01.Contains(4))
                        {
                            CurseTimeCounterDragon01 = SleepingCurseDuration;
                        }

                        break;
                
                    case 2:
                        
                        if (!Curse.PoolCurseDragon02.Contains(2))
                        {
                            MoveDownDragon02();
                        }
                        else
                        {
                            MoveUpDragon02();
                        }
                        
                        if (Curse.PoolCurseDragon02.Contains(4))
                        {
                            CurseTimeCounterDragon02 = SleepingCurseDuration;
                        }

                        break;
                
                    case 3:

                        if (!Curse.PoolCurseDragon03.Contains(2))
                        {
                            MoveDownDragon03();
                        }
                        else
                        {
                            MoveUpDragon03();
                        }
                        
                        if (Curse.PoolCurseDragon03.Contains(4))
                        {
                            CurseTimeCounterDragon03 = SleepingCurseDuration;
                        }

                        break;
                }
            }
        }
        
        public void MoveRight()
        {
            if (!isMoving)
            {
                AudioMove.Play();
                
                switch (ChooseDragon.DragonUnderControl)
                {
                    case 1:

                        if (!Curse.PoolCurseDragon01.Contains(1))
                        {
                            MoveRightDragon01();
                        }
                        else
                        {
                            MoveLeftDragon01();
                        }
                        
                        if (Curse.PoolCurseDragon01.Contains(4))
                        {
                            CurseTimeCounterDragon01 = SleepingCurseDuration;
                        }

                        break;
                
                    case 2:
                        
                        if (!Curse.PoolCurseDragon02.Contains(1))
                        {
                            MoveRightDragon02();
                        }
                        else
                        {
                            MoveLeftDragon02();
                        }
                        
                        if (Curse.PoolCurseDragon02.Contains(4))
                        {
                            CurseTimeCounterDragon02 = SleepingCurseDuration;
                        }

                        break;
                
                    case 3:

                        if (!Curse.PoolCurseDragon03.Contains(1))
                        {
                            MoveRightDragon03();
                        }
                        else
                        {
                            MoveLeftDragon03();
                        }
                        
                        if (Curse.PoolCurseDragon03.Contains(4))
                        {
                            CurseTimeCounterDragon03 = SleepingCurseDuration;
                        }
                    
                        break;
                }
            }
        }
        
        public void MoveLeft()
        {
            AudioMove.Play();
            
            if (!isMoving)
            {
                switch (ChooseDragon.DragonUnderControl)
                {
                    case 1:
                        
                        if (!Curse.PoolCurseDragon01.Contains(1))
                        {
                            MoveLeftDragon01();
                        }
                        else
                        {
                            MoveRightDragon01();
                        }
                        
                        if (Curse.PoolCurseDragon01.Contains(4))
                        {
                            CurseTimeCounterDragon01 = SleepingCurseDuration;
                        }

                        break;
                
                    case 2:
                        
                        if (!Curse.PoolCurseDragon02.Contains(1))
                        {
                            MoveLeftDragon02();
                        }
                        else
                        {
                            MoveRightDragon02();
                        }
                        
                        if (Curse.PoolCurseDragon02.Contains(4))
                        {
                            CurseTimeCounterDragon02 = SleepingCurseDuration;
                        }

                        break;
                
                    case 3:
                    
                        if (!Curse.PoolCurseDragon03.Contains(1))
                        {
                            MoveLeftDragon03();
                        }
                        else
                        {
                            MoveRightDragon03();
                        }
                        
                        if (Curse.PoolCurseDragon03.Contains(4))
                        {
                            CurseTimeCounterDragon03 = SleepingCurseDuration;
                        }

                        break;
                }
            }
        }

        private void MoveUpDragon01()
        {
            aimingPosition = new Vector3(PositionCurrentDragon01.x, PositionCurrentDragon01.y, PositionCurrentDragon01.z + moveDistance);

            if (DragonAttack.Dragon01Enemy == null)
            {
                dragon01.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon01.z != moveDistance && aimingPosition != PositionCurrentDragon02 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon01 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon01 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveDownDragon01()
        {
            aimingPosition = new Vector3(PositionCurrentDragon01.x, PositionCurrentDragon01.y, PositionCurrentDragon01.z - moveDistance);

            if (DragonAttack.Dragon01Enemy == null)
            {
                dragon01.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon01.z != -moveDistance && aimingPosition != PositionCurrentDragon02 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon01 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon01 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveRightDragon01()
        {
            aimingPosition = new Vector3(PositionCurrentDragon01.x + moveDistance, PositionCurrentDragon01.y, PositionCurrentDragon01.z);

            if (DragonAttack.Dragon01Enemy == null)
            {
                dragon01.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon01.x != moveDistance && aimingPosition != PositionCurrentDragon02 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);
                    
                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon01 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon01 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveLeftDragon01()
        {
            aimingPosition = new Vector3(PositionCurrentDragon01.x - moveDistance, PositionCurrentDragon01.y, PositionCurrentDragon01.z);

            if (DragonAttack.Dragon01Enemy == null)
            {
                dragon01.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon01.x != -moveDistance && aimingPosition != PositionCurrentDragon02 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon01 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon01 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon01.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveUpDragon02()
        {
            aimingPosition = new Vector3(PositionCurrentDragon02.x, PositionCurrentDragon02.y, PositionCurrentDragon02.z + moveDistance);

            if (DragonAttack.Dragon02Enemy == null)
            {
                dragon02.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon02.z != moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon02 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon02 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveDownDragon02()
        {
            aimingPosition = new Vector3(PositionCurrentDragon02.x, PositionCurrentDragon02.y, PositionCurrentDragon02.z - moveDistance);

            if (DragonAttack.Dragon02Enemy == null)
            {
                dragon02.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon02.z != -moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon02 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon02 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveRightDragon02()
        {
            aimingPosition = new Vector3(PositionCurrentDragon02.x + moveDistance, PositionCurrentDragon02.y, PositionCurrentDragon02.z);

            if (DragonAttack.Dragon02Enemy == null)
            {
                dragon02.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon02.x != moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon02 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon02 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveLeftDragon02()
        {
            aimingPosition = new Vector3(PositionCurrentDragon02.x - moveDistance, PositionCurrentDragon02.y, PositionCurrentDragon02.z);

            if (DragonAttack.Dragon02Enemy == null)
            {
                dragon02.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon02.x != -moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon03)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon02 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon02 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon02.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveUpDragon03()
        {
            aimingPosition = new Vector3(PositionCurrentDragon03.x, PositionCurrentDragon03.y, PositionCurrentDragon03.z + moveDistance);

            if (DragonAttack.Dragon03Enemy == null)
            {
                dragon03.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon03.z != moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon02)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon03 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon03 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveDownDragon03()
        {
            aimingPosition = new Vector3(PositionCurrentDragon03.x, PositionCurrentDragon03.y, PositionCurrentDragon03.z - moveDistance);

            if (DragonAttack.Dragon03Enemy == null)
            {
                dragon03.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon03.z != -moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon02)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon03 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon03 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveRightDragon03()
        {
            aimingPosition = new Vector3(PositionCurrentDragon03.x + moveDistance, PositionCurrentDragon03.y, PositionCurrentDragon03.z);

            if (DragonAttack.Dragon03Enemy == null)
            {
                dragon03.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon03.x != moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon02)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon03 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon03 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
        
        private void MoveLeftDragon03()
        {
            aimingPosition = new Vector3(PositionCurrentDragon03.x - moveDistance, PositionCurrentDragon03.y, PositionCurrentDragon03.z);

            if (DragonAttack.Dragon03Enemy == null)
            {
                dragon03.transform.LookAt(aimingPosition);
            }

            if (PositionCurrentDragon03.x != -moveDistance && aimingPosition != PositionCurrentDragon01 && aimingPosition != PositionCurrentDragon02)
            {
                if (RiseObstacle.PoolObstacleActive.Count > 0)
                {
                    Vector3 plannarAimingPosition = new Vector3(aimingPosition.x, 0.0f, aimingPosition.z);

                    for (int i = 0; i < RiseObstacle.PoolObstacleActive.Count; i++)
                    {
                        Vector3 plannarObstaclePosition = new Vector3(RiseObstacle.PoolObstacleActive[i].transform.position.x, 0.0f, RiseObstacle.PoolObstacleActive[i].transform.position.z);

                        if (plannarAimingPosition == plannarObstaclePosition)
                        {
                            dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
                                
                            break;
                        }
                        else if (RiseObstacle.PoolObstacleActive.Count < i + 2)
                        {
                            PositionAimingDragon03 = aimingPosition;
                            isMoving = true;
                        }
                    }
                }
                else
                {
                    PositionAimingDragon03 = aimingPosition;
                    isMoving = true;
                }
            }
            else
            {
                dragon03.GetComponentInChildren<Animator>().Play("get_hit_front");
            }
        }
    }
}