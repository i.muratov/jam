﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public class EnemyDetector : MonoBehaviour
    {
        private void OnTriggerStay(Collider other)
        {
            if (transform.parent.CompareTag("Dragon01") && DragonAttack.Dragon01Enemy == null)
            {
                if (Curse.Dragon01ConnectWith == 1)
                {
                    if (other.gameObject.CompareTag("Enemy01")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 2)
                {
                    if (other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 3)
                {
                    if (other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 12 || Curse.Dragon01ConnectWith == 21)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 13 || Curse.Dragon01ConnectWith == 31)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 23 || Curse.Dragon01ConnectWith == 32)
                {
                    if (other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
                else if (Curse.Dragon01ConnectWith == 123 || Curse.Dragon01ConnectWith == 132 || Curse.Dragon01ConnectWith == 213 || Curse.Dragon01ConnectWith == 231 || Curse.Dragon01ConnectWith == 312 || Curse.Dragon01ConnectWith == 321)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon01(other.gameObject);
                }
            }

            if (transform.parent.CompareTag("Dragon02") && DragonAttack.Dragon02Enemy == null)
            {
                if (Curse.Dragon02ConnectWith == 1)
                {
                    if (other.gameObject.CompareTag("Enemy01")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 2)
                {
                    if (other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 3)
                {
                    if (other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 12 || Curse.Dragon02ConnectWith == 21)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 13 || Curse.Dragon02ConnectWith == 31)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 23 || Curse.Dragon02ConnectWith == 32)
                {
                    if (other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
                else if (Curse.Dragon02ConnectWith == 123 || Curse.Dragon02ConnectWith == 132 || Curse.Dragon02ConnectWith == 213 || Curse.Dragon02ConnectWith == 231 || Curse.Dragon02ConnectWith == 312 || Curse.Dragon02ConnectWith == 321)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon02(other.gameObject);
                }
            }

            if (transform.parent.CompareTag("Dragon03") && DragonAttack.Dragon03Enemy == null)
            {
                if (Curse.Dragon03ConnectWith == 1)
                {
                    if (other.gameObject.CompareTag("Enemy01")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 2)
                {
                    if (other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 3)
                {
                    if (other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 12 || Curse.Dragon03ConnectWith == 21)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 13 || Curse.Dragon03ConnectWith == 31)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 23 || Curse.Dragon03ConnectWith == 32)
                {
                    if (other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
                else if (Curse.Dragon03ConnectWith == 123 || Curse.Dragon03ConnectWith == 132 || Curse.Dragon03ConnectWith == 213 || Curse.Dragon03ConnectWith == 231 || Curse.Dragon03ConnectWith == 312 || Curse.Dragon03ConnectWith == 321)
                {
                    if (other.gameObject.CompareTag("Enemy01") || other.gameObject.CompareTag("Enemy02") || other.gameObject.CompareTag("Enemy03")) DragonAttack.StartAttackDragon03(other.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (transform.parent.CompareTag("Dragon01") && other.gameObject == DragonAttack.Dragon01Enemy) DragonAttack.Dragon01Enemy = null;
            if (transform.parent.CompareTag("Dragon02") && other.gameObject == DragonAttack.Dragon02Enemy) DragonAttack.Dragon02Enemy = null;
            if (transform.parent.CompareTag("Dragon03") && other.gameObject == DragonAttack.Dragon03Enemy) DragonAttack.Dragon03Enemy = null;
        }
    }
}