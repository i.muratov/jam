﻿using UnityEngine;

namespace Core.Scripts
{
    public class TurnOffExplosion : MonoBehaviour
    {
        private ParticleSystem explosion;

        private void Start()
        {
            explosion = GetComponent<ParticleSystem>();
        }

        private void FixedUpdate()
        {
            if (!explosion.IsAlive())
            {
                gameObject.SetActive(false);
            }
        }
    }
}