﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Scripts
{
    public class Tutorial : MonoBehaviour
    {
        [SerializeField] private GameObject panel;
        [SerializeField] private GameObject page01;
        [SerializeField] private GameObject page02;
        [SerializeField] private GameObject page03;
        [SerializeField] private GameObject page04;
        [SerializeField] private GameObject page05;
        
        public static bool IsTutorialOver;

        public void OnTap()
        {
            if (page01.activeInHierarchy)
            {
                page01.SetActive(false);
            }
            else
            {
                if (page02.activeInHierarchy)
                {
                    page02.SetActive(false);
                }
                else
                {
                    if (page03.activeInHierarchy)
                    {
                        page03.SetActive(false);
                    }
                    else
                    {
                        if (page04.activeInHierarchy)
                        {
                            page04.SetActive(false);
                        }
                        else
                        {
                            if (page05.activeInHierarchy)
                            {
                                page05.SetActive(false);
                                
                                panel.GetComponent<Animator>().Play("FadeOut");

                                StartCoroutine(Wait());
                            }
                        }
                    }
                }
            }
            
            
            Debug.Log("Tap");
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(1.0f);
            
            gameObject.SetActive(false);
            IsTutorialOver = true;
        }
    }
}