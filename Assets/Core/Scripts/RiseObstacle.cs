﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Scripts
{
    public class RiseObstacle : MonoBehaviour
    {
        [SerializeField] private GameObject[] obstacles;
        [SerializeField] private int waveToUseObstacle01;
        [SerializeField] private int waveToUseObstacle02;
        [SerializeField] private float obstacleTimeLive;
        [SerializeField] private float obstacleTimeDelay;
        
        public static readonly List<GameObject> PoolObstacleActive = new List<GameObject>();
        
        private readonly List<GameObject> poolObstacle = new List<GameObject>();

        private void Awake()
        {
            for (int i = 0; i < obstacles.Length; i++)
            {
                poolObstacle.Add(obstacles[i]);
            }
        }

        private void Start()
        {
            StartCoroutine(Rise());
        }

        private IEnumerator Rise()
        {
            while (!DragonMove.isGameOver)
            {
                yield return new WaitForSeconds(obstacleTimeDelay);
                
                Vector3 plannarPositionDragon01 = new Vector3(DragonMove.PositionAimingDragon01.x, 0.0f, DragonMove.PositionAimingDragon01.z);
                Vector3 plannarPositionDragon02 = new Vector3(DragonMove.PositionAimingDragon02.x, 0.0f, DragonMove.PositionAimingDragon02.z);
                Vector3 plannarPositionDragon03 = new Vector3(DragonMove.PositionAimingDragon03.x, 0.0f, DragonMove.PositionAimingDragon03.z);

                int countObstacle;

                if (EnemySpawn.WaveCounter < waveToUseObstacle01)
                {
                    countObstacle = 0;
                }
                else if (EnemySpawn.WaveCounter < waveToUseObstacle02)
                {
                    countObstacle = 1;
                }
                else
                {
                    countObstacle = 2;
                }
                
                Shuffle();

                if (countObstacle > 0)
                {
                    int count = 0;
                    
                    for (int i = 0; i < poolObstacle.Count; i++)
                    {
                        Vector3 plannarPositionObstacle = new Vector3(poolObstacle[i].transform.position.x, 0.0f, poolObstacle[i].transform.position.z);

                        if (plannarPositionObstacle != plannarPositionDragon01 && plannarPositionObstacle != plannarPositionDragon02 && plannarPositionObstacle != plannarPositionDragon03)
                        {
                            if (count < countObstacle)
                            {
                                poolObstacle[i].GetComponent<Animator>().Play("Up");
                        
                                PoolObstacleActive.Add(poolObstacle[i]);
                            
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                
                yield return new WaitForSeconds(obstacleTimeLive);

                foreach (var obstacle in PoolObstacleActive)
                {
                    obstacle.GetComponent<Animator>().Play("Down");
                }
                
                PoolObstacleActive.Clear();
            }
        }
        
        private void Shuffle()
        {
            for (int i = 0; i < poolObstacle.Count; i++)
            {
                GameObject temp = poolObstacle[i];
                int randomIndex = Random.Range(i, poolObstacle.Count);
                poolObstacle[i] = poolObstacle[randomIndex];
                poolObstacle[randomIndex] = temp;
            }
        }
    }
}